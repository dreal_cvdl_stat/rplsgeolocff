# Géolocalisation RPLS

Travaux sur la geolocalisation de rpls par les fichiers fonciers réalisés par

- Murielle Lethrosne

# rplsgeolocff

*rplsgeolocff* est un package qui permet le pré-traitement des fichiers rpls et fonciers au niveau des adresses et ainsi  le rapprochement de ces 2 fichiers


## Installation à partir de gitlab dans RStudio :

``` r
remotes::install_gitlab("dreal_cvdl_stat/rplsgeolocff")
```

## Exemple d'utilisation du package
``` r
library(rplsgeolocff)
ff<-fread("d45_2018_pb0010_local.csv"),
          colClasses = c("idcom"="character","dindic"="character","dnvoiri"="character"))

rpls<-fread("rpls2019_loi_reg24.csv",sep=";",
            colClasses = c("DEP"="character","DEPCOM"="character","IDENT_REP"="character"),
            encoding="UTF-8")

malist<-prepa(rpls,ff,DEP=45)
liste_resultat<-geolocaliser_rpls(malist[[1]],malist[[2]])
tabmerge_reussi_FINAL<-liste_resultat[[2]]
tabmerge_echoue_FINAL<-liste_resultat[[1]]

#write.table(tabmerge_reussi_FINAL,"DPT45_RPLS2019_FF2018_GEOLOC.csv",sep=",",row.names = FALSE,quote=FALSE)
#write.table(tabmerge_echoue_FINAL,"DPT45_RPLS2019_FF2018_NONGEOLOC.csv",sep=",",row.names = FALSE,quote=FALSE)

```

