#' TERME
#' permet l uniformisation des types de voiries ainsi que des indices
#' @param dataset data.frame
#' @param myvar nom de la variable
#' @importFrom dplyr mutate
#' @importFrom magrittr %>%
#' @importFrom stringr  str_replace_all str_squish
#' @return data.frame
#' @export
#'
#' @examples
#' TERME(ff,dnvoiri)
TERME<-function(dataset,myvar){
  myvar <- enquo(myvar) # Pour 'quoter' la variable
  datas<-dataset %>%
    mutate(var_adresse2=str_squish(!!myvar),
           # TERME */

           var_adresse2=str_replace_all(var_adresse2,"BIS_","B_"),
           var_adresse2=str_replace_all(var_adresse2,"TER_","T_"),
           var_adresse2=str_replace_all(var_adresse2,"_QUATER ","Q_"),
           var_adresse2=str_replace_all(var_adresse2,"_BIS ","B_"),
           var_adresse2=str_replace_all(var_adresse2,"_TER ","T_"),

           var_adresse2=str_replace_all(var_adresse2,"_ALLEE ","_ALL "),var_adresse2=str_replace_all(var_adresse2,"_ALLEEE ","_ALL "),var_adresse2=str_replace_all(var_adresse2,"_ALLE ","_ALL "),var_adresse2=str_replace_all(var_adresse2,"_AL ","_ALL "),
           var_adresse2=str_replace_all(var_adresse2,"ALL_","_ALL "),var_adresse2=str_replace_all(var_adresse2," ALLEE "," ALL "),
           var_adresse2=str_replace_all(var_adresse2,"_ALTERRASSES ","_ALL TERRASSES "),var_adresse2=str_replace_all(var_adresse2,"ALLEE_PETITE ","_ALL PETITE "),
           var_adresse2=str_replace_all(var_adresse2,"_ALL ALL ","_ALL "),


           var_adresse2=str_replace_all(var_adresse2,"ANCIENNE ROUTE","ANCIENNE RTE"),
           var_adresse2=str_replace_all(var_adresse2,"RTE ANCIENNE RTE","ART ANCIENNE RTE"),
           var_adresse2=str_replace_all(var_adresse2,"_ANCIENNE RTE","_ART ANCIENNE RTE"),
           var_adresse2=str_replace_all(var_adresse2,"_ART ART ","_ART "),

           var_adresse2=str_replace_all(var_adresse2,"AVENUE ","AV "),var_adresse2=str_replace_all(var_adresse2,"_AVE ","_AV "),
           var_adresse2=str_replace_all(var_adresse2,"_AV AV ","_AV "),

           var_adresse2=str_replace_all(var_adresse2,"BOULEVARD ","BD "),var_adresse2=str_replace_all(var_adresse2,"BVD ","BD "),var_adresse2=str_replace_all(var_adresse2,"BLD","BD"),
           var_adresse2=str_replace_all(var_adresse2,"BD BD ","BD "),

           var_adresse2=str_replace_all(var_adresse2,"CARREFOUR","CAR"),
           var_adresse2=str_replace_all(var_adresse2,"CAR CAR ","CAR"),

           var_adresse2=str_replace_all(var_adresse2,"CHAMP ","CHP "),var_adresse2=str_replace_all(var_adresse2,"CHAMPS ","CHP "),
           var_adresse2=str_replace_all(var_adresse2,"CHP CHP ","CHP "),

           var_adresse2=str_replace_all(var_adresse2,"CHEMIN ","CHE "),var_adresse2=str_replace_all(var_adresse2,"CHEM ","CHE "),var_adresse2=str_replace_all(var_adresse2,"_CH ","_CHE "),var_adresse2=str_replace_all(var_adresse2,"CHEMI ","CHE "),
           var_adresse2=str_replace_all(var_adresse2,"CHE CHE ","CHE "),

           var_adresse2=str_replace_all(var_adresse2,"CLO ","CLOS "),var_adresse2=str_replace_all(var_adresse2,"LOT. CLOS ","CLOS "),var_adresse2=str_replace_all(var_adresse2,"CLOS CLOS ","CLOS "),

           var_adresse2=str_replace_all(var_adresse2,"_CLOITRE ","_CLOI "),

           var_adresse2=str_replace_all(var_adresse2,"CIT ","CITE "),var_adresse2=str_replace_all(var_adresse2,"CITES ","CITE "),var_adresse2=str_replace_all(var_adresse2,"CITE CITE ","CITE "),

           var_adresse2=str_replace_all(var_adresse2,"_COURS ","_CRS "),var_adresse2=str_replace_all(var_adresse2,"_COUR ","_CRS "),var_adresse2=str_replace_all(var_adresse2,"_CO ","_CRS "),
           var_adresse2=str_replace_all(var_adresse2,"COURS COUR ","CRS "),var_adresse2=str_replace_all(var_adresse2,"CRS COUR ","CRS "),var_adresse2=str_replace_all(var_adresse2," COURS ","CRS "),
           var_adresse2=str_replace_all(var_adresse2,"_CRS CRS ","_CRS "),

           var_adresse2=str_replace_all(var_adresse2,"FAUB ","FG "),var_adresse2=str_replace_all(var_adresse2,"FAUBOURG ","FG "),var_adresse2=str_replace_all(var_adresse2,"FBG ","FG "),var_adresse2=str_replace_all(var_adresse2,"FB ","FG "),
           var_adresse2=str_replace_all(var_adresse2,"FG FG ","FG "),

           var_adresse2=str_replace_all(var_adresse2,"HAMEAU ","HAM "),

           var_adresse2=str_replace_all(var_adresse2,"IMPASSE ","IMP "),var_adresse2=str_replace_all(var_adresse2,"IMPAS ","IMP "),var_adresse2=str_replace_all(var_adresse2,"IMPASE ","IMP "),
           var_adresse2=str_replace_all(var_adresse2,"IMP IMP ","IMP "),

           var_adresse2=str_replace_all(var_adresse2,"_LIEU DIT LIEUDIT ","_LD "),var_adresse2=str_replace_all(var_adresse2,"_LIEU DIT ","_LD "),var_adresse2=str_replace_all(var_adresse2,"_LD LD ","_LD "),

           var_adresse2=str_replace_all(var_adresse2,"LOTISSEMENT ","LOT "),var_adresse2=str_replace_all(var_adresse2,"LOTISSEM ","LOT "),var_adresse2=str_replace_all(var_adresse2,"LOTIS ","LOT "),var_adresse2=str_replace_all(var_adresse2,"LOT LOT ","LOT "),

           var_adresse2=str_replace_all(var_adresse2,"_PASSAGE ","_PASS "),var_adresse2=str_replace_all(var_adresse2,"_PAS ","_PASS "),var_adresse2=str_replace_all(var_adresse2,"_PASS PAS ","_PASS "),

           var_adresse2=str_replace_all(var_adresse2,"PLACETTE ","PTTE "),var_adresse2=str_replace_all(var_adresse2,"PLACE ","PL "),var_adresse2=str_replace_all(var_adresse2,"PCE ","PL "),var_adresse2=str_replace_all(var_adresse2,"PL PL","PL"),
           var_adresse2=str_replace_all(var_adresse2,"_PLE ","_PL "),var_adresse2=str_replace_all(var_adresse2,"_PLDU ","_PL "),

           var_adresse2=str_replace_all(var_adresse2,"PRO ","PROM "),var_adresse2=str_replace_all(var_adresse2,"_PROMENADE ","_PROM "),

           var_adresse2=str_replace_all(var_adresse2,"_QU ","_QUAI "),

           var_adresse2=str_replace_all(var_adresse2,"RESIDENCE ","RES "),var_adresse2=str_replace_all(var_adresse2,"RESIDENCES ","RES "),var_adresse2=str_replace_all(var_adresse2,"REIDENCE ","RES "),
           var_adresse2=str_replace_all(var_adresse2,"_RESID ","_RES "),var_adresse2=str_replace_all(var_adresse2,"_RES RES ","_RES "),

           var_adresse2=str_replace_all(var_adresse2,"ROCADE ","ROC "),

           var_adresse2=str_replace_all(var_adresse2,"_RD POINT ","_RPT"),var_adresse2=str_replace_all(var_adresse2,"_RONDD POINT ","_RPT"),var_adresse2=str_replace_all(var_adresse2,"_ROND POINT ","_RPT"),

           var_adresse2=str_replace_all(var_adresse2,"_RT ","_RTE "),var_adresse2=str_replace_all(var_adresse2,"ROUTE ","RTE "),var_adresse2=str_replace_all(var_adresse2,"RTE RTE ","RTE "),

           var_adresse2=str_replace_all(var_adresse2,"_R ","_RUE "),var_adresse2=str_replace_all(var_adresse2,"R_","_RUE "),var_adresse2=str_replace_all(var_adresse2,"_RU ","_RUE "),
           var_adresse2=str_replace_all(var_adresse2,"_RUR ","_RUE "),var_adresse2=str_replace_all(var_adresse2,"RUE_","_RUE "),var_adresse2=str_replace_all(var_adresse2,"_RUE GDE RUE ","_GDE RUE "),
           var_adresse2=str_replace_all(var_adresse2,"_RUE GRANDE RUE","_GDE RUE"),var_adresse2=str_replace_all(var_adresse2,"RUE RUE","RUE"),

           var_adresse2=str_replace_all(var_adresse2,"RUEL ","RLE "),var_adresse2=str_replace_all(var_adresse2,"RUELLE ","RLE "),
           var_adresse2=str_replace_all(var_adresse2,"RLE RLE ","RLE "),

           var_adresse2=str_replace_all(var_adresse2,"SQUARE ","SQ "),var_adresse2=str_replace_all(var_adresse2,"_SQ SQ ","_SQ "),

           var_adresse2=str_replace_all(var_adresse2,"TERRASSE ","TSSE "),

           var_adresse2=str_replace_all(var_adresse2,"VENELLE ","VEN "),var_adresse2=str_replace_all(var_adresse2,"VENE ","VEN "),var_adresse2=str_replace_all(var_adresse2,"_VEN VEN ","_VEN "),

           var_adresse2=str_replace_all(var_adresse2,"_VOI ","_VOIE "))
  return(datas)
}
